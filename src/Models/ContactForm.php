<?php
// CA\PackageContact\src\Models\ContactForm.php

namespace CA\PackageContact\Models;

use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
    protected $guarded = [];
    protected $table = 'contact';
}