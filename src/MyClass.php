<?php

namespace CA\PackageContact;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class MyClass
{
    public static function postUpdate(Event $event)
    {
        $composer = $event->getComposer();
        // do stuff
        $file1=fopen("CA\PackageContact\src\routes\web.php","w");
        fwrite($file1,"this is roseindia",30);
    }

    public static function postAutoloadDump(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        require $vendorDir . '/autoload.php';

        some_function_from_an_autoloaded_file();
    }

    public static function postPackageInstall(PackageEvent $event)
    {
        $installedPackage = $event->getOperation()->getPackage();
        // do stuff
        echo "hello";
        // die;
    }

    public static function warmCache(Event $event)
    {
        // make cache toasty
    }
}